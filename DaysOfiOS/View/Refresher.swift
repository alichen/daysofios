//
//  Refresher.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/4.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit
import Cartography

extension RefresherViewController{
    func renderTableView(){
        let tableview = self.tableViewController.tableView
        tableview.backgroundColor = UIColor.magentaColor()
        tableview.tableFooterView = UIView(frame: CGRectZero)
        tableview.separatorStyle = .None
        //        self.tableview.registerNib(UINib(nibName: "RefresherCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableViewController.tableView.registerClass(RefresherCell.self, forCellReuseIdentifier: "cell")
        tableview.estimatedRowHeight = 60.0
        tableview.rowHeight = UITableViewAutomaticDimension
        
        self.refresherControl.tintColor = UIColor.grayColor()
        self.refresherControl.attributedTitle = NSAttributedString(string: "last update on \(NSDate())", attributes: [NSForegroundColorAttributeName:UIColor.greenColor()])
        self.tableViewController.refreshControl = self.refresherControl
        
        self.view.addSubview(tableview)
        constrain(tableview){view in
            //            view.width == view.superview!.width
            view.edges == inset(view.superview!.edges, 60, 10, 10, 10)
            //            view.height == view.superview!.height
        }
    }
    
}

class RefresherCell:UITableViewCell{
    var newTitle:String!{
        didSet{
            self.updateUI()
        }
    }
    
    var textlabel:UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        constrain(self){ view in
            //            view.width == view.superview!.width
            //            view.height == view.superview!.height
            //            view.trailing == view.superview!.trailing + 10
        }
        self.textlabel = UILabel(frame: CGRect(x: 1, y: 1, width: 50, height: 50))
        self.textLabel?.textAlignment = NSTextAlignment.Center
        self.backgroundColor = UIColor.greenColor()
        self.addSubview(textlabel)
        
    }
    
    func updateUI(){
        self.textlabel.text = newTitle
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
