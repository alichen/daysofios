//
//  Interest.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/3.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class Interest{
    var title:String?
    var featuredImage:UIImage?
    
    class func sharedInstance(title:String?,featuredImage:UIImage?)->Interest{
        struct InstanceContainer{
            static var instance:Interest? = nil
            static var predicate:dispatch_once_t = 0
        }
        
        dispatch_once(&InstanceContainer.predicate){()->Void in
            InstanceContainer.instance = Interest(title: title, featuredImage: featuredImage)
        }
        return InstanceContainer.instance!
    }
    
    init(title:String?,featuredImage:UIImage?){
        self.title = title
        self.featuredImage = featuredImage
    }
    
    static func interests()->[Interest]{
        return [
            Interest(title:"1",featuredImage:UIImage(named: "hello")),
            Interest(title:"2",featuredImage:UIImage(named: "hhhhh")),
            Interest(title:"3",featuredImage:UIImage(named: "dudu"))
        ]
    }
}
