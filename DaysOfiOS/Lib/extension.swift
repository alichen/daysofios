//
//  extension.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/5.
//  Copyright © 2016年 chenli. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    @IBInspectable var cornerRadius:CGFloat{
        get{
            return layer.cornerRadius
        }
        set{
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth:CGFloat{
        get{
            return layer.borderWidth
        }
        set{
            layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor:UIColor?{
        set{
            layer.borderColor = borderColor?.CGColor
        }
        get{
            return layer.borderColor != nil ?UIColor(CGColor: layer.borderColor!) :nil
        }
    }
}
