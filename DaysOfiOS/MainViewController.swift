//
//  ViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/1.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    @IBOutlet var tableview:UITableView!
    var apps:[UIViewController]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apps = [
            StopWatchViewController.init(nibName:"StopWatch",bundle:nil),
            LocalvideoViewController.init(nibName:"LocalVideo",bundle:nil),
            SnapchatViewController(nibName:"Snapchat",bundle:nil),
            InterestViewController(nibName:"Interest",bundle:nil),
            RefresherViewController(nibName:"Refresher",bundle:nil),
            LocalAudioViewController(nibName:"LocalAudio",bundle:nil),
            ImageViewController(nibName:"ImageView",bundle:nil),
            SpotifyViewController(nibName:"Spotify",bundle:nil),
            SplashViewController(nibName:"Splash",bundle:nil),
            //UIStoryboard(name: "Interest", bundle: nil).instantiateViewControllerWithIdentifier("interest")
        ]
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - table view delegate
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apps!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableview.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        cell.textLabel?.text = apps![indexPath.row].nibName
        return cell
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = apps![indexPath.row]
        //        let vc:UIViewController = UIViewController.init(nibName: "StopWatch", bundle: nil)
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    
}

