//
//  SpotifyViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/5.
//  Copyright © 2016年 chenli. All rights reserved.
//


import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class SpotifyViewController: UIViewController {
    var moviePlayer = AVPlayerViewController()
    var videoFrame = CGRect()
    
    @IBOutlet weak var button:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.backgroundColor = UIColor.greenColor()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        moviePlayer.view.frame = view.bounds
        moviePlayer.showsPlaybackControls = false
        moviePlayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        view.addSubview(moviePlayer.view)
        view.sendSubviewToBack(moviePlayer.view)
        setPlayer()
        self.moviePlayer.player?.play()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("playBack"), name: AVPlayerItemDidPlayToEndTimeNotification, object: moviePlayer.player?.currentItem)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func setPlayer(){
        let url = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("moments", ofType: "mp4")!)
        let player = AVPlayer(URL: url)
        player.volume = 1.0
        self.moviePlayer.player = player
    }
    
    func playBack(){
        moviePlayer.player?.seekToTime(kCMTimeZero)
        moviePlayer.player?.play()
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
