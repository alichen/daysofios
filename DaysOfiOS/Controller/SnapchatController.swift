//
//  ScrollViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/2.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class SnapchatViewController: UIViewController {
    
    @IBOutlet weak var scrollview:UIScrollView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        let leftVC = LeftViewController()
        let centerVC = CenterViewController()
        let rightVC = RightViewController()
        
        self.scrollview?.addSubview(leftVC.view)
        self.scrollview?.addSubview(centerVC.view)
        self.scrollview?.addSubview(rightVC.view)
        
        
        centerVC.view.frame.origin.x = self.view.frame.width
        rightVC.view.frame.origin.x = self.view.frame.width * 2
        //print(self.scrollview?.frame.width,self.view.frame.width,leftVC.view.frame.width)
        self.scrollview?.contentSize = CGSizeMake(self.view.frame.width * 3, self.view.frame.height)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

class CenterViewController:UIViewController{
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.yellowColor()
    }
    
}

class LeftViewController:UIViewController{
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.redColor()
    }
}

class RightViewController:UIViewController{
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.blueColor()
    }
    
}