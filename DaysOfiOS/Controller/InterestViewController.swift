//
//  InterestViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/3.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class InterestViewController: UIViewController {
    
    var interests:[Interest] = Interest.interests()
    @IBOutlet weak var collectionView:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

extension InterestViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let nib = UINib.init(nibName: "InterestCell", bundle: nil)
        self.collectionView.registerNib(nib, forCellWithReuseIdentifier: "cell")
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! InterestCell
        cell.interest = self.interests[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.interests.count
    }
}

class InterestCell:UICollectionViewCell{
    @IBOutlet weak var label:UILabel!
    @IBOutlet weak var imageview:UIImageView!
    var interest:Interest!{
        didSet{
            self.updateUI()
        }
    }
    
    func updateUI(){
        self.label.text = interest.title
        self.imageview.image = interest.featuredImage
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
    }
}
