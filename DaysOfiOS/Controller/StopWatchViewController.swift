//
//  StopWatchViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/1.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class StopWatchViewController: UIViewController {
    @IBOutlet var label:UILabel!
    
    var timer:NSTimer = NSTimer()
    var isPlaying = false
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "\(counter)"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startTimer(sender:AnyObject){
        if isPlaying {
            return
        }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("UpdateTimer"), userInfo: nil, repeats: true)
        isPlaying = true
    }
    
    @IBAction func resetTimer(sender:AnyObject){
        timer.invalidate()
        counter = 0
        isPlaying = false
        label.text = "\(counter)"
    }
    
    
    @IBAction func pauseTimer(sender:AnyObject){
        timer.invalidate()
        isPlaying = false
    }
    
    func UpdateTimer(){
        counter += 1
        label.text = "\(counter)"
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
