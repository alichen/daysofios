//
//  LocalAudioViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/5.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit
import AVFoundation

class LocalAudioViewController: UIViewController {
    @IBOutlet weak var playButton:UIButton!
    
    var audioPlayer = AVAudioPlayer()
    
    var gradientLayer = CAGradientLayer()
    var timer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        audioPlayer.numberOfLoops = -1
        let mp3 = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Ecstasy", ofType: "mp3")!)
        do{
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            try audioPlayer = AVAudioPlayer(contentsOfURL: mp3)
            audioPlayer.prepareToPlay()
        }catch let audioError as NSError{
            print("\(audioError)")
        }
        self.randomColor()
        gradientLayer.frame = view.bounds
        let color1 = UIColor(white: 0.5, alpha: 0.2).CGColor as CGColorRef
        let color2 = UIColor(red: 1.0, green: 0, blue: 0, alpha: 0.4).CGColor as CGColorRef
        let color3 = UIColor(red: 0, green: 1, blue: 0, alpha: 0.3).CGColor as CGColorRef
        let color4 = UIColor(red: 0, green: 0, blue: 1, alpha: 0.3).CGColor as CGColorRef
        let color5 = UIColor(white: 0.4, alpha: 0.2).CGColor as CGColorRef
        
        gradientLayer.colors = [color1, color2, color3, color4, color5]
        gradientLayer.locations = [0.10, 0.30, 0.50, 0.70, 0.90]
        gradientLayer.startPoint = CGPointMake(0, 0)
        gradientLayer.endPoint = CGPointMake(1, 1)
        self.view.layer.addSublayer(gradientLayer)
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func spin(){
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.playButton.transform = CGAffineTransformRotate(self.playButton.transform, CGFloat(M_PI))
            //                self.playButton.layer.transform = CATransform3DMakeRotation(10.0, 0, 0, 0)
            },completion: { (Bool)->Void in
                self.spin()
        })
    }
    
    @IBAction func didPlay(sender:UIButton){
        if audioPlayer.playing {
            audioPlayer.pause()
            timer.invalidate()
            self.spin()
        }else{
            timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("randomColor"), userInfo: nil, repeats: true)
            audioPlayer.play()
        }
    }
    
    func randomColor(){
        let red = CGFloat(drand48())
        let blue = CGFloat(drand48())
        let green = CGFloat(drand48())
        
        self.view.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
