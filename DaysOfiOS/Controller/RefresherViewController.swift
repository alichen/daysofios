//
//  RefresherViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/4.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class RefresherViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    let tableViewController = UITableViewController(style: .Plain)
    
    let refresherControl = UIRefreshControl()
    
    //    @IBOutlet weak var tableview:UITableView!
    var titles:Array<String> = ["a","b","c"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.renderTableView()
        //        self.navigationItem.title = "Refresher"
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "More", style:UIBarButtonItemStyle.Done, target: self, action: nil)
        self.refresherControl.addTarget(self, action: Selector("didRefresh:"), forControlEvents: .ValueChanged)
        
        self.tableViewController.tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableViewController.tableView.reloadData()
        let cells = self.tableViewController.tableView.visibleCells
        let tableHeight = self.tableViewController.tableView.bounds.height
        for i  in cells{
            let cell:UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        var index:Int = 0
        for a in cells{
            let cell:UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1    , delay: 0.05 * Double(index), usingSpringWithDamping: 9, initialSpringVelocity: 0, options: [], animations: { () -> Void in
                cell.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: { (Bool) -> Void in
                    //                    print("finish")
            })
            index++
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableViewController.tableView.dequeueReusableCellWithIdentifier(
            "cell", forIndexPath: indexPath) as! RefresherCell
        cell.newTitle = titles[indexPath.row]
        //        cell.textlabel.text = titles[indexPath.row]
        //        let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        //        cell.backgroundColor = UIColor.greenColor()
        //        cell.textLabel?.text = titles[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func didRefresh(sender:AnyObject){
        titles += ["d","e"]
        self.tableViewController.tableView.reloadData()
        self.refresherControl.endRefreshing()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let done  = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Done") { (UITableViewRowAction, NSIndexPath) -> Void in
            //            let alertview = UIAlertView(title: "info", message: "row: \(NSIndexPath.row)", delegate: self, cancelButtonTitle: "ok", otherButtonTitles: nil)
            print(NSIndexPath.row)
        }
        return [done]
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
