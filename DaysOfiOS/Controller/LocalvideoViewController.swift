//
//  LocalvideoViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/2.
//  Copyright © 2016年 chenli. All rights reserved.
//


import UIKit
import AVKit
import AVFoundation

class LocalvideoViewController: UIViewController {
    
    var playViewController:AVPlayerViewController = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func play(sender:AnyObject){
        let path = NSBundle.mainBundle().pathForResource("emoji zone", ofType: "mp4")
        let url =  NSURL(fileURLWithPath: path!)
        let playView = AVPlayer(URL: url)
        self.playViewController.player = playView
        self.presentViewController(playViewController, animated: true) { () -> Void in
            self.playViewController.player?.play()
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
