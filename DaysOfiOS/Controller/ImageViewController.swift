//
//  ImageViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/5.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController,UIScrollViewDelegate {
    var scrollview:UIScrollView!
    var imageview:UIImageView!
    
    override func viewDidLoad() {
        imageview = UIImageView(image: UIImage(named:"hello" ))
        initScrollView()
        scrollview.delegate = self
    }
    
    private func initScrollView(){
        scrollview = UIScrollView(frame: view.bounds)
        scrollview.autoresizingMask = [.FlexibleWidth,.FlexibleHeight]
        scrollview.contentSize = imageview.bounds.size
        scrollview.backgroundColor = UIColor.clearColor()
        scrollview.addSubview(imageview)
        view.addSubview(scrollview)
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        self.setupImage()
    }
    
    private func setupImage(){
        let scrollViewSize = scrollview.bounds.size
        let imageViewSize = imageview.bounds.size
        let horizontalSpace = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2.0 : 0
        let verticalSpace = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.width) / 2.0 :0
        
        scrollview.contentInset = UIEdgeInsetsMake(verticalSpace, horizontalSpace, verticalSpace, horizontalSpace)
        
    }
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageview
    }
}
