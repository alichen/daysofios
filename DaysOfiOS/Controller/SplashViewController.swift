//
//  SplashViewController.swift
//  DaysOfiOS
//
//  Created by chenli on 16/3/5.
//  Copyright © 2016年 chenli. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    var mask:CALayer = CALayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        mask.contents = UIImage(named: "splash")?.CGImage
        mask.bounds = CGRectMake(0, 0, 100, 81)
        mask.contentsGravity = kCAGravityResizeAspectFill
        mask.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        mask.position = CGPoint(x: view.bounds.size.width / 2, y: view.bounds.size.height / 2)
        self.view.layer.mask = mask
        animateMask()
    }
    
    private func animateMask(){
        let keyAnimation = CAKeyframeAnimation(keyPath: "bounds")
        keyAnimation.duration = 0.6
        keyAnimation.delegate = self
        keyAnimation.beginTime = CACurrentMediaTime() + 0.5
        keyAnimation.timingFunctions = [
            CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut),
            CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        ]
        let firstBounds = NSValue(CGRect:mask.bounds)
        let secondBounds = NSValue(CGRect:CGRectMake(0, 0, 90, 73))
        let thirdBounds = NSValue(CGRect:CGRectMake(0, 0, 1600, 1300))
        
        keyAnimation.values = [firstBounds,secondBounds,thirdBounds]
        keyAnimation.keyTimes = [0,0.3,1]
        self.mask.addAnimation(keyAnimation, forKey: "bounds")
        
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        self.view.layer.mask = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
